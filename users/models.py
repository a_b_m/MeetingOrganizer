from django.db import models


class User(models.Model):
    id = models.IntegerField()
    username = models.CharField(max_length=200, primary_key=True)
    password = models.CharField(max_length=200)
    email = models.CharField(max_length=200)
    phone_number = models.CharField(max_length=12)
